export const GetHealthStatusContract = Object.freeze({
  type: "get-health-status",
  status: "stable",
  path: "/internal/health",
  schema: {
    v1: {
      props: {},
      response: {
        status: {
          type: "string",
        },
      },
    },
  },
});
