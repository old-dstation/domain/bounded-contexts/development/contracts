import { GetHealthStatusContract } from "./write-model/internal/queries/get-health-status";

export const Contracts = Object.freeze({
  writeModel: {
    internal: {
      queries: {
        getHealthStatus: GetHealthStatusContract,
      },
    },
  },
});
